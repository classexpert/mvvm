package ru.classexpert.mvvm;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.databinding.ViewDataBinding;
import android.os.Handler;
import android.support.annotation.CallSuper;
import android.widget.Toast;

public abstract class BaseViewModel<T extends ViewDataBinding, TV extends ViewInterface> extends BaseObservable {

    protected T _binding;

    public T getBinding() { return _binding; }

    private Handler _handler = new Handler();
    private Thread _uiThread;
    protected TV _view;

    public BaseViewModel(final T binding, TV view)
    {
        init(binding,view);
        ViewModelsUtil.getInstance().add(this);
    }


    @CallSuper
    public void reattach(final T binding, ViewInterface view)
    {
        init(binding, view);
    }

    private void init(final T binding, ViewInterface view)
    {
        _binding=binding;
        _view=(TV)view;
        _uiThread=Thread.currentThread();
    }

    public Context getContext()
    {
        return _binding.getRoot().getContext();
    }

    public void Toast(String msg)
    {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public final void runOnUiThread(Runnable action) {
        if(Thread.currentThread() != _uiThread) {
            _handler.post(action);
        } else {
            action.run();
        }
    }

    protected Resources getResources() {
        if(getContext() == null)
            return null;
        return getContext().getResources();
    }

    @CallSuper
    public void finish()
    {
        //ViewModelsUtil.getInstance().remove(this);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {}

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {}

    public void onLoaded() {
    }
}