package ru.classexpert.mvvm;

import android.databinding.BindingConversion;
import android.view.View;

/**
 * Created by o on 21.03.2016.
 */
public class Convert {
    @BindingConversion
    public static int booleanToVisibility(boolean value) {
        return value ? View.VISIBLE : View.GONE;
    }

    @BindingConversion
    public static int negativeBooleanToVisibility(boolean value) {
        return value ? View.GONE : View.VISIBLE;
    }
}
