package ru.classexpert.mvvm;

import android.app.Activity;
import android.databinding.ViewDataBinding;

/**
 * Created by o on 21.03.2016.
 */
public interface ViewInterface<TB extends ViewDataBinding, TM extends BaseViewModel> {
    TM getViewModel();

    TB getBinding();

    Activity getActivity();
}
