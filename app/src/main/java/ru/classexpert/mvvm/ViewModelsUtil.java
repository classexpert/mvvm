package ru.classexpert.mvvm;

import android.databinding.ViewDataBinding;

import java.util.WeakHashMap;

/**
 * Created by o on 21.03.2016.
 */
public class ViewModelsUtil {
    private static ViewModelsUtil _instance;
    private WeakHashMap<Class, BaseViewModel> _models=new WeakHashMap<>();

    public static ViewModelsUtil getInstance()
    {
        if(_instance==null)
            _instance=new ViewModelsUtil();
        return _instance;
    }

    public void add(BaseViewModel model)
    {
        _models.put(model.getClass(),model);
    }

    public <T extends BaseViewModel> T get(Class<T> model)
    {
        return (T)_models.get(model);
    }

    public <T extends ViewDataBinding> void remove(BaseViewModel model) { _models.remove(model.getClass());
    }
}
