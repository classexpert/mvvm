package ru.classexpert.mvvm;

import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.support.v4.app.Fragment;

public abstract class BaseFragment<TB extends ViewDataBinding, TM extends BaseViewModel> extends Fragment implements ViewInterface {
    protected TB binding;
    protected TM model;

    @Override
    public void onDestroy() {
        model.finish();
        super.onDestroy();
    }

    public TM getViewModel() {
        return model;
    }

    public TB getBinding() {
        return binding;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        model.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

}
