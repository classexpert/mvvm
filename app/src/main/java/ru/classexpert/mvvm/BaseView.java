package ru.classexpert.mvvm;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;


public abstract class BaseView<TB extends ViewDataBinding, TM extends BaseViewModel> extends AppCompatActivity implements ViewInterface{
    protected TB binding;
    protected TM model;

    @Override
    public void onDestroy() {
        loaded=false;
        model.finish();
        super.onDestroy();
    }

    public TM getViewModel() {
        return model;
    }

    public TB getBinding() {
        return binding;
    }


    public Activity getActivity()
    {
        return this;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {  //android:configChanges="orientation|screenSize"/>
        loaded=false;
        model.reattach(binding, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        model.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        model.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private boolean loaded=false;
    @Override public void  onWindowFocusChanged (boolean hasFocus)
    {
        if(!loaded) {
            loaded=true;
            model.onLoaded();
        }
    }
}
